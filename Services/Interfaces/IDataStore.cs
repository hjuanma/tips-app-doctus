﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
   public interface IDataStore<T> where T : class
   {
      Task<int> AddTip (T item);

      Task RemoveTip(int Id);

      Task<IEnumerable<T>> GetTips();

   }
}
