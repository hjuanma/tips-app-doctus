﻿using Models;
using SQLite;

namespace Services.Models
{
   public class TipDB : Tip
   {
      [PrimaryKey, AutoIncrement]
      public int Id { get; set; }
   }
}
