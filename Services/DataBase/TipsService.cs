﻿using Services.Interfaces;
using Services.Models;
using SQLite;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.DataBase
{
   public class TipsService : IDataStore<TipDB>
   {
      private static SQLiteAsyncConnection db;
      private async Task Init()
      {
         if (db != null)
         {
            return;
         }

         db = new SQLiteAsyncConnection(Constants.ConstantsDB.DatabasePath);
         await db.CreateTableAsync<TipDB>();
      }

      public async Task<int> AddTip(TipDB item)
      {
         await Init();
         return await db.InsertAsync(item);
      }

      public async Task<IEnumerable<TipDB>> GetTips()
      {
         await Init();
         return await db.Table<TipDB>().ToListAsync();
      }


      public async Task RemoveTip(int Id)
      {
         await Init();
         await db.DeleteAsync<TipDB>(Id);
      }

   }
}
