﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
   public class Tip
   {
      public DateTime CreateDate { get; set; } = DateTime.Now;

      public string Title { get; set; } = string.Empty;
      public string Description { get; set; } = string.Empty;
   }
}
