﻿using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tips_app_Doctus.Views;
using Xamarin.Forms;

namespace Tips_app_Doctus.ViewModels
{
   public class TipsDetailViewModel : BaseViewModel
   {
      public Command EditCommand { get; }
      public Command DeleteCommand { get; }

      private TipDB tip;
      public TipDB Tip
      {
         get { return tip; }
         set { SetProperty(ref tip, value); }
      }

      public TipsDetailViewModel()
      {
         EditCommand = new Command(EditItem);
         DeleteCommand = new Command(Delete);

      }

      private async void Delete()
      {
         await DataStore.RemoveTip(Tip.Id);
         await view.DisplayAlert("Guardado", $"se a ELIMINADO el tip {Tip.Title}", "OK");
         await App.Current.MainPage.Navigation.PopToRootAsync();
      }

      private async void EditItem()
      {
         var view = new AddTip();
         view.viewModel.Tip = Tip;
         await App.Current.MainPage.Navigation.PushAsync(view);
      }
   }
}
