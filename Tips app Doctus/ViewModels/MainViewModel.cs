﻿using Models;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Tips_app_Doctus.Views;
using Xamarin.Forms;

namespace Tips_app_Doctus.ViewModels
{
   public class MainViewModel : BaseViewModel
   {
      public Command AddItemCommand { get; }

      private ObservableCollection<TipDB> tipsList;
      public ObservableCollection<TipDB> TipsList
      {
         get { return tipsList; }
         set { SetProperty(ref tipsList, value); }
      }


      public MainViewModel()
      {

         AddItemCommand = new Command(OnAddItem);
      }

      private async void OnAddItem(object obj)
      {
         var view = new AddTip();
         await App.Current.MainPage.Navigation.PushAsync(view);
      }

      public async void RefreshTips()
      {
         var tips = await DataStore.GetTips();

         TipsList = new ObservableCollection<TipDB>(tips.ToList().OrderByDescending(t => t.CreateDate));
      }

   }
}
