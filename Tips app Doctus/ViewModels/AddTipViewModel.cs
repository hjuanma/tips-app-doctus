﻿using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Tips_app_Doctus.ViewModels
{
   public class AddTipViewModel : BaseViewModel
   {

      private TipDB tip = new TipDB();
      public TipDB Tip
      {
         get { return tip; }
         set { SetProperty(ref tip, value); }
      }

      public Command CommandSave { get; }

      public AddTipViewModel()
      {
         CommandSave = new Command(SaveTip);
      }

      private async void SaveTip()
      {
         var add = await DataStore.AddTip(Tip);
         if (add != 0)
         {
            await view.DisplayAlert("Guardado", $"se a GUARDADO el tip {Tip.Title}", "OK");
            await App.Current.MainPage.Navigation.PopToRootAsync();
         }
      }

   }
}
