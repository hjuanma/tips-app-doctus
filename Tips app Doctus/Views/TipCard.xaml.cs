﻿
using Services.Interfaces;
using Services.Models;
using Tips_app_Doctus.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tips_app_Doctus.Views
{
   [XamlCompilation(XamlCompilationOptions.Compile)]
   public partial class TipCard : ContentView
   {

      public TipDB Tip
      {
         get { return (TipDB)GetValue(TipProperty); }
         set { SetValue(TipProperty, value); }
      }


      public static readonly BindableProperty TipProperty =
         BindableProperty.Create(nameof(Tip),
                                 typeof(TipDB),
                                 typeof(TipCard));

      public TipCard()
      {
         InitializeComponent();
      }

      private async void OpenClicked(object sender, System.EventArgs e)
      {
         var view = new TipDetail();
         view.vm.Tip = Tip;
         await App.Current.MainPage.Navigation.PushAsync(view);

      }
      private async void DeleteClicked(object sender, System.EventArgs e)
{
         await BaseViewModel.DataStore.RemoveTip(Tip.Id);
         await App.Current.MainPage.DisplayAlert("Guardado", $"se a ELIMINADO el tip {Tip.Title}", "OK");
         ((MainPage)App.Current.MainPage.Navigation.NavigationStack[0]).vm.RefreshTips();
         await App.Current.MainPage.Navigation.PopToRootAsync();

      }
   }
}