﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tips_app_Doctus.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tips_app_Doctus.Views
{
   [XamlCompilation(XamlCompilationOptions.Compile)]
   public partial class AddTip : ContentPage
   {
      public AddTipViewModel viewModel;
      public AddTip()
      {
         InitializeComponent();
         BindingContext = viewModel = new AddTipViewModel() { view = this };
      }
   }
}