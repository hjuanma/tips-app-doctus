﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tips_app_Doctus.ViewModels;
using Xamarin.Forms;

namespace Tips_app_Doctus.Views
{
   public partial class MainPage : ContentPage
   {

      public MainViewModel vm;

      public MainPage()
      {
         InitializeComponent();
         BindingContext = vm = new MainViewModel() { view = this };
      }

      protected override void OnAppearing()
      {
         base.OnAppearing();
         vm.RefreshTips();
      }
   }
}
