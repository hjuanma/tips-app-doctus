﻿using Services.DataBase;
using System;
using Tips_app_Doctus.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tips_app_Doctus
{
   public partial class App : Application
   {

      public App()
      {
         InitializeComponent();

         DependencyService.Register<TipsService>();
         MainPage = new NavigationPage(new MainPage());
      }

      protected override void OnStart()
      {
      }

      protected override void OnSleep()
      {
      }

      protected override void OnResume()
      {
      }
   }
}
